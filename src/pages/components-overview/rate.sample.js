// material-ui
import { 
    Typography,
    Button
} from '@mui/material';
import { Component } from 'react';

// project import
import MainCard from 'components/MainCard';
import { RateRequest } from 'apis/xerate.api';

// ==============================|| SAMPLE PAGE ||============================== //

/*const RateSamplePage = () => (
  <MainCard title="Sample Card">
    <Typography variant="body2">
      Lorem ipsum dolor sit amen, consenter nipissing eli, sed do elusion tempos incident ut laborers et doolie magna alissa. Ut enif ad
      minim venice, quin nostrum exercitation illampu laborings nisi ut liquid ex ea commons construal. Duos aube grue dolor in reprehended
      in voltage veil esse colum doolie eu fujian bulla parian. Exceptive sin ocean cuspidate non president, sunk in culpa qui officiate
      descent molls anim id est labours.
    </Typography>
  </MainCard>
);*/

class RateSamplePage extends Component {

    constructor(props) {
        super(props);
        this.state = {favoritecolor: null};
    }
    
    sampleApi = async () => {
        let result = await RateRequest();
        console.log(result.data.symbols);
        this.setState({favoritecolor: result.data.symbols});
    }

    render() {
        return (
            <MainCard title="Sample Rate Page">
                <Typography variant="body2">
                    {JSON.stringify(this.state.favoritecolor)}
                    <Button variant="contained" onClick={this.sampleApi}>Click me</Button>
                </Typography>
            </MainCard>
        );
      }
}

export default RateSamplePage;
