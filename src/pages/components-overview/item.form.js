// material-ui
import { 
    Typography,
    Button,
    InputLabel,
    OutlinedInput,
    Grid,
    Stack
} from '@mui/material';
import { Component } from 'react';
import AnimateButton from 'components/@extended/AnimateButton';

// project import
import MainCard from 'components/MainCard';
//import {RateRequest} from 'apis/xerate.api';
import socket from 'socket/socket';
import ConnectionManager from 'socket/ConnectionManager';

// ==============================|| SAMPLE PAGE ||============================== //

/*const RateSamplePage = () => (
  <MainCard title="Sample Card">
    <Typography variant="body2">
      Lorem ipsum dolor sit amen, consenter nipissing eli, sed do elusion tempos incident ut laborers et doolie magna alissa. Ut enif ad
      minim venice, quin nostrum exercitation illampu laborings nisi ut liquid ex ea commons construal. Duos aube grue dolor in reprehended
      in voltage veil esse colum doolie eu fujian bulla parian. Exceptive sin ocean cuspidate non president, sunk in culpa qui officiate
      descent molls anim id est labours.
    </Typography>
  </MainCard>
);*/

class ItemSamplePage extends Component {
    
    constructor(props) {
        super(props);
        //[this.inputs, this.setInputs] = useState();
        this.state = {firstname: ''};

    }
    
    sampleApi = async () => {
        let result = await RateRequest();
        console.log(result.data.symbols);
        this.setState({firstname: result.data.symbols});
    }

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value});
    }

    handleSubmit = () => {
        alert(JSON.stringify(this.state.firstname));
    }

    handleSocketMessage = () => {
        //socket.emit('news', 'eddie test');
        socket.emit('news', this.state.firstname);
    }


    render() {
        return (
            <MainCard title="Sample Item Page">
                <Typography variant="body3">
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <Stack spacing={1}>
                                <InputLabel htmlFor="firstname-item">Item Name*</InputLabel>
                                <OutlinedInput
                                    id="firstname-item"
                                    type="firstname"
                                    value={this.state.firstname}
                                    name="firstname"
                                    //onBlur={handleBlur}
                                    onChange={this.handleChange}
                                    placeholder="Item 1"
                                    fullWidth
                                    //error={Boolean(touched.firstname && errors.firstname)}
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <AnimateButton>
                                <Button disableElevation disabled={false} fullWidth size="large" type="submit" variant="contained" color="primary" onClick={this.handleSubmit}>
                                    Save
                                </Button>
                            </AnimateButton>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <AnimateButton>
                                <Button disableElevation disabled={false} fullWidth size="large" type="submit" variant="contained" color="primary" onClick={this.handleSocketMessage}>
                                    Send Message to WebSocket Server
                                </Button>
                            </AnimateButton>
                        </Grid>
                        <Grid item xs={12}>
                            <ConnectionManager />
                        </Grid>
                    </Grid>
                </Typography>
            </MainCard>
        );
      }
}

export default ItemSamplePage;
