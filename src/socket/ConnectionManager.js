import socket from './socket';
import { Component } from 'react';
import { Button, Grid } from '@mui/material';

class ConnectionManager extends Component {
    constructor(props) {
        super(props);
        //[this.inputs, this.setInputs] = useState();
        this.state = {firstname: ''};

    }
    connect = () => {
        socket.connect();
        console.log('The socket is ' + socket.connected + ' with id: ' + socket.id);
    }
    disconnect = () => {
        socket.disconnect();
        console.log('The socket is ' + socket.connected + ' with id: ' + socket.id);
    }

    render() {
        return (
            <Grid container spacing={3}>
                <Grid item xs={12} md={6}>
                    <Button disableElevation disabled={false} fullWidth size="large" type="connect" variant="contained" color="primary" onClick={this.connect}>
                        Connect
                    </Button>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Button disableElevation disabled={false} fullWidth size="large" type="disconnect" variant="contained" color="primary" onClick={this.disconnect}>
                        Disconnect
                    </Button>
                </Grid>
            </Grid>
        );
    }
}

export default ConnectionManager;