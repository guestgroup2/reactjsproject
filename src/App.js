// project import
import Routes from 'routes';
import ThemeCustomization from 'themes';
import ScrollTop from 'components/ScrollTop';


// third-party
//import { useDispatch, useSelector } from 'react-redux';

// apex-chart
import 'assets/third-party/apex-chart.css';

// project import
//import socket from 'socket/socket';
//import { addNotification } from 'store/reducers/menu';

// ==============================|| APP - THEME, ROUTER, LOCAL  ||============================== //

const App = () => (
  <ThemeCustomization>
    <ScrollTop>
      <Routes />
    </ScrollTop>
  </ThemeCustomization>
);

export default App;
