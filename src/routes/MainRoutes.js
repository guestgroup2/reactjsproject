import { lazy } from 'react';

// project import
import Loadable from 'components/Loadable';
import MainLayout from 'layout/MainLayout';

// render - dashboard
const DashboardDefault = Loadable(lazy(() => import('pages/dashboard')));

const RateExploreForm = Loadable(lazy(() => import('pages/components-overview/xerate')));
const RateSamplePage = Loadable(lazy(() => import('pages/components-overview/rate.sample')));
const ItemSamplePage = Loadable(lazy(() => import('pages/components-overview/item.form')));
// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  path: '/',
  element: <MainLayout />,
  children: [
    {
      path: '/',
      element: <DashboardDefault />
    },
    {
      path: 'dashboard',
      children: [
        {
          path: 'default',
          element: <DashboardDefault />
        }
      ]
    },
    {
      path: 'rate',
      element: <RateExploreForm />
    },
    {
      path: 'rate-page',
      element: <RateSamplePage />
    },
    {
      path: 'item-page',
      element: <ItemSamplePage />
    }
  ]
};

export default MainRoutes;
