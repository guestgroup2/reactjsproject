//import axios from 'axios';
import axiosService from 'utils/axiosService';

const RateRequest = async () =>{
    return await axiosService.get('http://localhost:5000/xerate/symbols');
};

export {
    RateRequest
};