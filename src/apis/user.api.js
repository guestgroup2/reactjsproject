//import axios from 'axios';
import axiosService from 'utils/axiosService';

const RegisterRequest = async (user) =>{
    return await axiosService.post('http://localhost:5000/user/reg', user);
};

export {
    RegisterRequest
};