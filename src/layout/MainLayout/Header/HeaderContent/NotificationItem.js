import { /*useRef, useState,*/ Component } from 'react';

// material-ui
//import { useTheme } from '@mui/material/styles';
import {
  Avatar,
  //Badge,
  //Box,
  //ClickAwayListener,
  Divider,
  //IconButton,
  //List,
  ListItemButton,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  //Paper,
  //Popper,
  Typography,
  //useMediaQuery
} from '@mui/material';

// project import
//import MainCard from 'components/MainCard';
//import Transitions from 'components/@extended/Transitions';

// assets
import { /*BellOutlined, CloseOutlined,*/ GiftOutlined, /*MessageOutlined, SettingOutlined*/ } from '@ant-design/icons';

/* sx styles
const avatarSX = {
  width: 36,
  height: 36,
  fontSize: '1rem'
};

const actionSX = {
  mt: '6px',
  ml: 1,
  top: 'auto',
  right: 'auto',
  alignSelf: 'flex-start',

  transform: 'none'
};
*/

class NotificationItem extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            item: {
                message: props.item.message,
                ago: props.item.ago,
                msg_datetime: props.item.datetime
            }
        };
    }
    
    render () {
        return (
            <div>
                <ListItemButton>
                    <ListItemAvatar>
                        <Avatar
                            sx={{
                            color: 'success.main',
                            bgcolor: 'success.lighter'
                            }}
                        >
                            <GiftOutlined />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={
                            <Typography variant="h6">
                                {this.state.item.message}
                            </Typography>
                        }
                        secondary={this.state.item.ago}
                    />
                    <ListItemSecondaryAction>
                        <Typography variant="caption" noWrap>
                            {this.state.item.msg_datetime}
                        </Typography>
                    </ListItemSecondaryAction>
                </ListItemButton>
                <Divider />
            </div>
        );
    }
}

export default NotificationItem