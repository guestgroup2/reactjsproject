import axios from 'axios'
/*import jwtDecode from 'jwt-decode'
import { CONSTANTS } from '../constants'
import { saveLocalStorage } from './saveLocalStorage'
import { parseJwt } from './tokenChecking'
import { accessTokenRequest } from '../apis/loginApi'
*/

class axiosService {
    constructor() {
        const instance = axios.create({ headers: { "Content-Type": "application/json;charset=utf-8" } })
        instance.interceptors.response.use(this.handleSuccess, this.handleError)
        /*instance.interceptors.request.use(async (config) => {
            if (!config.headers.Authorization) {
                let token = this.getToken('access_token');
                if (token) {
                    let isExpire = this.isTokenExpired(token)
                    if (!isExpire) {
                        config.headers.Authorization = token;
                    }
                    else {
                        let getRefreshToken = this.getToken('refresh_token')
                        await accessTokenRequest({ 'refreshToken': getRefreshToken })
                            .then((res) => {
                                let accessToken = res.data.accessToken;
                                let payload = parseJwt(accessToken);
                                saveLocalStorage(payload, accessToken);
                                config.headers.Authorization = res.data.accessToken;
                            })
                            .catch((err) => {
                                localStorage.clear()
                                window.location.href = CONSTANTS.url.login
                            })
                    }
                }
            }
            return config;
        },error=>{
            Promise.reject(error)
        });*/
        this.instance = instance;
    }

    /*isTokenExpired(token){
        const parts = token.split(`-`)
        if (parts[0].toUpperCase() === `JWT` && token.substring(4)) {
            if (jwtDecode(token).exp < Date.now() / 1000) {
                localStorage.clear()
                return true
            }
        }
        return false
    }

    getToken(tokenType){
        return localStorage.getItem(tokenType)
    }*/

    handleSuccess(response){
        console.log(response)
        return response
    }

    handleError(error){
        return Promise.reject(error.response)
    }

    async get(url){
        return await this.instance.get(url)
    }

    post(url, payload){
        return this.instance.post(url, payload)
    }

    put(url, payload){
        return this.instance.put(url, payload)
    }

    delete(url){
        return this.instance.delete(url)
    }
}

export default new axiosService()
