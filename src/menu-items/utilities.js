// assets
import {
  AppstoreAddOutlined,
  AntDesignOutlined,
  BarcodeOutlined,
  BgColorsOutlined,
  FontSizeOutlined,
  LoadingOutlined
} from '@ant-design/icons';

// icons
const icons = {
  FontSizeOutlined,
  BgColorsOutlined,
  BarcodeOutlined,
  AntDesignOutlined,
  LoadingOutlined,
  AppstoreAddOutlined
};

// ==============================|| MENU ITEMS - UTILITIES ||============================== //

const utilities = {
  id: 'utilities',
  title: 'Utilities',
  type: 'group',
  children: [
    {
      id: 'util-rate',
      title: 'Rate Form',
      type: 'item',
      url: '/rate',
      icon: icons.BgColorsOutlined
    },
    {
      id: 'util-rate-sample',
      title: 'Rate Sample Page',
      type: 'item',
      url: '/rate-page',
      icon: icons.BgColorsOutlined
    },
    {
      id: 'util-item-form',
      title: 'Retailer Item',
      type: 'item',
      url: '/item-page',
      icon: icons.BgColorsOutlined
    }
  ]
};

export default utilities;
